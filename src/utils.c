/**
 * @file    utils.c
 * @author  Paul Thomas
 * @date    5/24/2024
 * @brief
 *
 * @copyright
 * Copyright 2024 Paul Thomas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the “Software”), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "utils.h"
#include "../include/serial_message.h"
#include "crc16.h"

unsigned char smu_hex_to_char(unsigned char hex) {
    if ('0' <= hex && hex <= '9') {
        return hex - '0';
    }
    if ('A' <= hex && hex <= 'F') {
        return hex - 'A' + 10;
    }
    return 0;
}

unsigned char smu_char_to_hex(unsigned char val) {
    if (val < 10) {
        return '0' + val;
    }
    if (val < 16) {
        return 'A' + val - 10;
    }
    return '0';
}

unsigned char smu_char_buffer_to_char(unsigned char *buffer) {
    return smu_hex_to_char(buffer[0]) << 4 | smu_hex_to_char(buffer[1]);
}

unsigned short smu_char_buffer_to_short(unsigned char *buffer) {
    return smu_char_buffer_to_char(buffer) | smu_char_buffer_to_char(buffer + 2) << 8;
}
unsigned int smu_char_buffer_to_int(unsigned char *buffer) {
    return smu_char_buffer_to_char(buffer) | smu_char_buffer_to_char(buffer + 2) << 8 |
           smu_char_buffer_to_char(buffer + 4) << 16 | smu_char_buffer_to_char(buffer + 6) << 24;
}

void smu_char_to_char_buffer(unsigned char val, unsigned char *buffer) {
    buffer[0] = smu_char_to_hex(val >> 4);
    buffer[1] = smu_char_to_hex(val & 0xf);
}

void smu_short_to_char_buffer(unsigned short val, unsigned char *buffer) {
    smu_char_to_char_buffer(val & 0xff, buffer);
    smu_char_to_char_buffer(val >> 8, buffer + 2);
}
void smu_int_to_char_buffer(unsigned int val, unsigned char *buffer) {
    smu_char_to_char_buffer(val & 0xff, buffer);
    smu_char_to_char_buffer((val >> 8) & 0xff, buffer + 2);
    smu_char_to_char_buffer((val >> 16) & 0xff, buffer + 4);
    smu_char_to_char_buffer((val >> 24) & 0xff, buffer + 6);
}

int smu_is_signaling_char(unsigned char val) { return (val == SOH || val == STX || val == ETX || val == EOT) ? 1 : 0; }

int smu_is_hexadecimal_char(unsigned char val) {
    return ('0' <= val && val <= '9') || ('A' <= val && val <= 'F') ? 1 : 0;
}

int smu_is_acceptable_char(unsigned char val) {
    return smu_is_signaling_char(val) != 0 || smu_is_hexadecimal_char(val) != 0 ? 1 : 0;
}
int smu_is_header_valid(const serial_message_accumulator_t *accumulator) {
    int index = PACKET_POS_HEADER;
    if (accumulator->length < PACKET_POS_STX) {
        return 0;
    }
    for (; index < PACKET_POS_STX; ++index) {
        if (smu_is_hexadecimal_char(accumulator->buffer[index]) == 0) {
            return 0;
        }
    }
    return 1;
}
int smu_is_payload_size_valid(const serial_message_accumulator_t *accumulator, int is_message_complete) {
    unsigned short expected_payload_length = smu_char_buffer_to_short(accumulator->buffer + PACKET_POS_HEADER_SIZE);
    unsigned short received_payload_length =
            accumulator->length - PACKET_POS_PAYLOAD - (is_message_complete ? 2 + PACKET_SIZE_CRC : 0);
    return (int) expected_payload_length == (int) received_payload_length ? 1 : 0;
}

int smu_is_in_crc_span(const serial_message_accumulator_t *accumulator) {
    int index = 0;
    if (accumulator->length < PACKET_POS_ETX_MIN) {
        return 0;
    }
    for (; index <= PACKET_SIZE_CRC; ++index) {
        if (accumulator->buffer[accumulator->length - index] == ETX) {
            return 1;
        }
    }
    return 0;
}

int smu_is_crc_valid(const serial_message_accumulator_t *accumulator, int has_eot) {
    unsigned short computed_crc =
            crc16(0x0, accumulator->buffer, accumulator->length - PACKET_SIZE_CRC - (has_eot ? 1 : 0));
    unsigned short expected_crc = smu_char_buffer_to_short(accumulator->buffer + accumulator->length -
                                                           PACKET_POS_CRC_FROM_END - (has_eot ? 1 : 0));
    return expected_crc == computed_crc ? 1 : 0;
}
