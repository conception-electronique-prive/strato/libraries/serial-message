/**
 * @file    serial_message.h
 * @author  Paul Thomas
 * @date    5/24/2024
 * @brief
 *
 * @copyright
 * Copyright 2024 Paul Thomas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the “Software”), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "serial_message.h"
#include "accumulator_stage.h"
#include "crc16.h"
#include "utils.h"

/**
 * Packet structure
 * [SOH]              - 00
 * [H-ID b15-b12]     - 01
 * [H-ID b11-b8]      - 02
 * [H-ID b7-b4]       - 03
 * [H-ID b3-b0]       - 04
 * [H-PLSize b15-b12] - 05
 * [H-PLSize b11-b8]  - 06
 * [H-PLSize b7-b4]   - 07
 * [H-PLSize b3-b0]   - 08
 * [STX]              - 09
 * [Payload ... X]    - 10 -> Size is always even
 * [ETX]              - 10 + Payload size
 * [CRC b15-b12]      - 11 + Payload size
 * [CRC b11-b8]       - 12 + Payload size
 * [CRC b7-b4]        - 13 + Payload size
 * [CRC b3-b0]        - 14 + Payload size
 * [EOT]              - 15 + Payload size
 */

serial_message_conversion_result_t serial_message_make_packet(const unsigned char* message_buffer,
                                                              unsigned short message_buffer_length,
                                                              unsigned char* packet_buffer,
                                                              unsigned short* packet_buffer_length) {
    int index = 0;
    unsigned short payload_size = message_buffer_length << 1;
    unsigned int crc;
    if (*packet_buffer_length < (payload_size + PACKET_SIZE_MIN)) {
        return SMCR_ERROR_BUFFER_TOO_SMALL;
    }
    *packet_buffer_length = payload_size + PACKET_SIZE_MIN;

    packet_buffer[PACKET_POS_SOH] = SOH;
    smu_short_to_char_buffer(0, packet_buffer + PACKET_POS_HEADER_ID);
    smu_short_to_char_buffer(payload_size, packet_buffer + PACKET_POS_HEADER_SIZE);
    packet_buffer[PACKET_POS_STX] = STX;
    for (; index < message_buffer_length; ++index) {
        smu_char_to_char_buffer(message_buffer[index], packet_buffer + PACKET_POS_PAYLOAD + index * 2);
    }
    packet_buffer[*packet_buffer_length - PACKET_POS_ETX_FROM_END - 1] = ETX;
    crc = crc16(0, packet_buffer, *packet_buffer_length - PACKET_POS_ETX_FROM_END);
    smu_int_to_char_buffer(crc, packet_buffer + *packet_buffer_length - PACKET_POS_CRC_FROM_END - 1);
    packet_buffer[*packet_buffer_length - 1] = EOT;
    return SMCR_SUCCESS;
}

serial_message_conversion_result_t serial_message_decode_message(const serial_message_accumulator_t* accumulator,
                                                                 unsigned char* message_buffer,
                                                                 unsigned short* message_buffer_length) {
    int index = 0;
    unsigned short payload_length;
    if (smu_is_crc_valid(accumulator, 1) == 0 || smu_is_payload_size_valid(accumulator, 1) == 0) {
        return SMCR_ERROR_INVALID_ACCUMULATOR;
    }
    payload_length = smu_char_buffer_to_short(accumulator->buffer + PACKET_POS_HEADER_SIZE);
    if ((*message_buffer_length) << 1 < payload_length) {
        return SMCR_ERROR_BUFFER_TOO_SMALL;
    }
    *message_buffer_length = payload_length >> 1;
    for (; index < *message_buffer_length; ++index) {
        message_buffer[index] = smu_char_buffer_to_char(accumulator->buffer + (PACKET_POS_PAYLOAD + index * 2));
    }
    return SMCR_SUCCESS;
}


void serial_message_init_accumulator(serial_message_accumulator_t* accumulator, unsigned char* buffer,
                                     unsigned short length) {
    accumulator->buffer = buffer;
    accumulator->max_length = length;
    accumulator->length = 0;
    accumulator->_stage_handler = smu_serial_message_accumulator_stage_soh;
}

void serial_message_reset_accumulator(serial_message_accumulator_t* accumulator) {
    accumulator->length = 0;
    accumulator->_stage_handler = smu_serial_message_accumulator_stage_soh;
}

static int is_message_complete(serial_message_accumulator_t* accumulator) {
    return accumulator->_stage_handler == smu_serial_message_accumulator_stage_complete;
}

serial_message_accumulator_result_t serial_message_accumulate_byte(unsigned char byte,
                                                                   serial_message_accumulator_t* accumulator) {
    int result;
    if (is_message_complete(accumulator)) {
        return SMAR_HAS_MESSAGE;
    }
    if (accumulator->max_length == 0) {
        return SMAR_ERROR_BUFFER_FULL;
    }
    result = accumulator->_stage_handler(byte, accumulator);
    if (result == SMAR_NO_MESSAGE && accumulator->length == accumulator->max_length) {
        result = SMAR_ERROR_BUFFER_FULL;
    }
    return result;
}

serial_message_accumulator_result_t serial_message_accumulate_buffer(const unsigned char* buffer,
                                                                     unsigned short* buffer_length,
                                                                     serial_message_accumulator_t* accumulator) {
    unsigned int index = 0;
    if (is_message_complete(accumulator)) {
        return SMAR_HAS_MESSAGE;
    }
    for (; index < *buffer_length; ++index) {
        serial_message_accumulator_result_t result = serial_message_accumulate_byte(buffer[index], accumulator);
        if (result != SMAR_NO_MESSAGE) {
            *buffer_length = index + 1;
            return result;
        }
    }
    return SMAR_NO_MESSAGE;
}
