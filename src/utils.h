/**
 * @file    utils.h
 * @author  Paul Thomas
 * @date    5/24/2024
 * @brief
 *
 * @copyright
 * Copyright 2024 Paul Thomas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the “Software”), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef SERIAL_MESSAGE_UTILS_H
#define SERIAL_MESSAGE_UTILS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "../include/serial_message.h"

#define SOH ((char) 1)
#define STX ((char) 2)
#define ETX ((char) 3)
#define EOT ((char) 4)

#define PACKET_POS_SOH 0
#define PACKET_POS_HEADER 1
#define PACKET_POS_HEADER_ID 1
#define PACKET_POS_HEADER_SIZE 5
#define PACKET_POS_STX 9
#define PACKET_POS_PAYLOAD 10
#define PACKET_POS_ETX_MIN PACKET_POS_PAYLOAD
#define PACKET_POS_ETX_FROM_END 5
#define PACKET_POS_CRC_FROM_END 4

#define PACKET_SIZE_HEADER 8
#define PACKET_SIZE_CRC 4
#define PACKET_SIZE_MIN (PACKET_SIZE_HEADER + PACKET_SIZE_CRC + 4)

unsigned char smu_hex_to_char(unsigned char hex);
unsigned char smu_char_to_hex(unsigned char val);

unsigned char smu_char_buffer_to_char(unsigned char *buffer);
unsigned short smu_char_buffer_to_short(unsigned char *buffer);
unsigned int smu_char_buffer_to_int(unsigned char *buffer);

void smu_char_to_char_buffer(unsigned char val, unsigned char *buffer);
void smu_short_to_char_buffer(unsigned short val, unsigned char *buffer);
void smu_int_to_char_buffer(unsigned int val, unsigned char *buffer);

int smu_is_signaling_char(unsigned char val);
int smu_is_hexadecimal_char(unsigned char val);
int smu_is_acceptable_char(unsigned char val);
int smu_is_header_valid(const serial_message_accumulator_t *accumulator);
int smu_is_payload_size_valid(const serial_message_accumulator_t *accumulator, int is_message_complete);
int smu_is_in_crc_span(const serial_message_accumulator_t *accumulator);
int smu_is_crc_valid(const serial_message_accumulator_t *accumulator, int has_eot);

#ifdef __cplusplus
}
#endif

#endif
