/**
 * @file    accumulator_stage.c
 * @author  Paul Thomas
 * @date    5/24/2024
 * @brief
 *
 * @copyright
 * Copyright 2024 Paul Thomas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the “Software”), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "accumulator_stage.h"
#include "serial_message.h"
#include "utils.h"

int smu_serial_message_accumulator_stage_soh(unsigned char byte, void* obj) {
    if (byte == SOH) {
        serial_message_accumulator_t* accumulator = obj;
        accumulator->_stage_handler = smu_serial_message_accumulator_stage_header;
        accumulator->buffer[0] = byte;
        accumulator->length = 1;
    }
    return SMAR_NO_MESSAGE;
}

int smu_serial_message_accumulator_stage_header(unsigned char byte, void* obj) {
    serial_message_accumulator_t* accumulator = obj;
    if (accumulator->length < PACKET_SIZE_HEADER + PACKET_POS_HEADER) {
        if (smu_is_hexadecimal_char(byte) == 0) {
            return SMAR_ERROR_ILL_FORMED_HEADER;
        }
    } else if (byte == STX) {
        accumulator->_stage_handler = smu_serial_message_accumulator_stage_payload;
    } else {
        return SMAR_ERROR_ILL_SIZED_HEADER;
    }
    accumulator->buffer[accumulator->length] = byte;
    ++accumulator->length;
    return SMAR_NO_MESSAGE;
}

int smu_serial_message_accumulator_stage_payload(unsigned char byte, void* obj) {
    serial_message_accumulator_t* accumulator = obj;
    if (byte == ETX) {
        if (smu_is_payload_size_valid(accumulator, 0) == 0) {
            return SMAR_ERROR_ILL_SIZED_PAYLOAD;
        }
        accumulator->_stage_handler = smu_serial_message_accumulator_stage_crc;
    } else if (smu_is_hexadecimal_char(byte) == 0) {
        return SMAR_ERROR_ILL_FORMED_PAYLOAD;
    }
    accumulator->buffer[accumulator->length] = byte;
    ++accumulator->length;
    return SMAR_NO_MESSAGE;
}

int smu_serial_message_accumulator_stage_crc(unsigned char byte, void* obj) {
    serial_message_accumulator_t* accumulator = obj;
    if (byte == EOT) {
        if (accumulator->buffer[accumulator->length - PACKET_SIZE_CRC - 1] != ETX) {
            return SMAR_ERROR_ILL_SIZED_CRC;
        }
        if (smu_is_crc_valid(accumulator, 0) == 0) {
            return SMAR_ERROR_INVALID_CRC;
        }
        accumulator->buffer[accumulator->length] = byte;
        ++accumulator->length;
        accumulator->_stage_handler = smu_serial_message_accumulator_stage_complete;
        return SMAR_HAS_MESSAGE;
    }

    if (smu_is_in_crc_span(accumulator)) {
        if (smu_is_hexadecimal_char(byte) == 0) {
            return SMAR_ERROR_ILL_FORMED_CRC;
        }
    } else {
        return SMAR_ERROR_ILL_SIZED_CRC;
    }
    accumulator->buffer[accumulator->length] = byte;
    ++accumulator->length;
    return SMAR_NO_MESSAGE;
}

int smu_serial_message_accumulator_stage_complete(unsigned char b, void* obj) {
    (void)b;
    (void)obj;
    return SMAR_HAS_MESSAGE;
}
