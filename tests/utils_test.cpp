/**
 * @file    utils_test.cpp
 * @author  Paul Thomas
 * @date    5/24/2024
 * @brief
 *
 * @copyright
 * Copyright 2024 Paul Thomas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the “Software”), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "../src/utils.h"
#include <gtest/gtest.h>
#include "serial_message.h"

TEST(utils, hex_to_char) {
    EXPECT_EQ(smu_hex_to_char('0'), 0);
    EXPECT_EQ(smu_hex_to_char('1'), 1);
    EXPECT_EQ(smu_hex_to_char('2'), 2);
    EXPECT_EQ(smu_hex_to_char('3'), 3);
    EXPECT_EQ(smu_hex_to_char('4'), 4);
    EXPECT_EQ(smu_hex_to_char('5'), 5);
    EXPECT_EQ(smu_hex_to_char('6'), 6);
    EXPECT_EQ(smu_hex_to_char('7'), 7);
    EXPECT_EQ(smu_hex_to_char('8'), 8);
    EXPECT_EQ(smu_hex_to_char('9'), 9);
    EXPECT_EQ(smu_hex_to_char('A'), 10);
    EXPECT_EQ(smu_hex_to_char('B'), 11);
    EXPECT_EQ(smu_hex_to_char('C'), 12);
    EXPECT_EQ(smu_hex_to_char('D'), 13);
    EXPECT_EQ(smu_hex_to_char('E'), 14);
    EXPECT_EQ(smu_hex_to_char('F'), 15);

    for (char i = 0; i < '0'; ++i) {
        EXPECT_EQ(smu_hex_to_char(i), 0);
    }
    for (char i = ':'; i < 'A'; ++i) {
        EXPECT_EQ(smu_hex_to_char(i), 0);
    }
    for (char i = '['; i < 127; ++i) {
        EXPECT_EQ(smu_hex_to_char(i), 0);
    }
    EXPECT_EQ(smu_hex_to_char(127), 0);
}

TEST(utils, char_to_hex) {
    EXPECT_EQ(smu_char_to_hex(0x0), '0');
    EXPECT_EQ(smu_char_to_hex(0x1), '1');
    EXPECT_EQ(smu_char_to_hex(0x2), '2');
    EXPECT_EQ(smu_char_to_hex(0x3), '3');
    EXPECT_EQ(smu_char_to_hex(0x4), '4');
    EXPECT_EQ(smu_char_to_hex(0x5), '5');
    EXPECT_EQ(smu_char_to_hex(0x6), '6');
    EXPECT_EQ(smu_char_to_hex(0x7), '7');
    EXPECT_EQ(smu_char_to_hex(0x8), '8');
    EXPECT_EQ(smu_char_to_hex(0x9), '9');
    EXPECT_EQ(smu_char_to_hex(0xA), 'A');
    EXPECT_EQ(smu_char_to_hex(0xB), 'B');
    EXPECT_EQ(smu_char_to_hex(0xC), 'C');
    EXPECT_EQ(smu_char_to_hex(0xD), 'D');
    EXPECT_EQ(smu_char_to_hex(0xE), 'E');
    EXPECT_EQ(smu_char_to_hex(0xF), 'F');
    for (int i = 0x10; i < 0xFF; ++i) {
        EXPECT_EQ(smu_char_to_hex(i), '0');
    }
    EXPECT_EQ(smu_char_to_hex(0xFF), '0');
}

TEST(utils, char_buffer_to_char) {
    unsigned char buffer[2] = {'A', 'B'};
    EXPECT_EQ(smu_char_buffer_to_char(buffer), 0xAB);
}

TEST(utils, char_buffer_to_short) {
    unsigned char buffer[4] = {'C', 'D', 'A', 'B'};
    EXPECT_EQ(smu_char_buffer_to_short(buffer), 0xABCD);
}

TEST(utils, char_buffer_to_int) {
    unsigned char buffer[8] = {'B', 'A', 'D', 'C', 'C', 'D', 'A', 'B'};
    EXPECT_EQ(smu_char_buffer_to_int(buffer), 0xABCDDCBA);
}

TEST(utils, char_to_char_buffer) {
    unsigned char buffer[2] = {0, 0};
    smu_char_to_char_buffer(0xAB, buffer);
    EXPECT_EQ(buffer[0], 'A');
    EXPECT_EQ(buffer[1], 'B');
}

TEST(utils, short_to_char_buffer) {
    unsigned char buffer[4] = {0, 0, 0, 0};
    smu_short_to_char_buffer(0xABCD, buffer);
    EXPECT_EQ(buffer[0], 'C');
    EXPECT_EQ(buffer[1], 'D');
    EXPECT_EQ(buffer[2], 'A');
    EXPECT_EQ(buffer[3], 'B');
}

TEST(utils, int_to_char_buffer) {
    unsigned char buffer[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    smu_int_to_char_buffer(0xABCDDCBA, buffer);
    EXPECT_EQ(buffer[0], 'B');
    EXPECT_EQ(buffer[1], 'A');
    EXPECT_EQ(buffer[2], 'D');
    EXPECT_EQ(buffer[3], 'C');
    EXPECT_EQ(buffer[4], 'C');
    EXPECT_EQ(buffer[5], 'D');
    EXPECT_EQ(buffer[6], 'A');
    EXPECT_EQ(buffer[7], 'B');
}
//
// TEST(utils, hex_buffer_to_char_buffer) {
//     {
//         // Valid
//         unsigned char hex_buffer[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
//         unsigned short hex_buffer_length = 16;
//         unsigned char char_buffer[10] = {};
//         unsigned short char_buffer_length = 10;
//         smu_hex_buffer_to_char_buffer(hex_buffer, hex_buffer_length, char_buffer, &char_buffer_length);
//         EXPECT_EQ(char_buffer_length, 8);
//         EXPECT_EQ(char_buffer[0], 0x01);
//         EXPECT_EQ(char_buffer[1], 0x23);
//         EXPECT_EQ(char_buffer[2], 0x45);
//         EXPECT_EQ(char_buffer[3], 0x67);
//         EXPECT_EQ(char_buffer[4], 0x89);
//         EXPECT_EQ(char_buffer[5], 0xAB);
//         EXPECT_EQ(char_buffer[6], 0xCD);
//         EXPECT_EQ(char_buffer[7], 0xEF);
//     }
//
//     {
//         // Nothing to convert
//         unsigned char hex_buffer[16] = {};
//         unsigned short hex_buffer_length = 0;
//         unsigned char char_buffer[8] = {};
//         unsigned short char_buffer_length = 8;
//         smu_hex_buffer_to_char_buffer(hex_buffer, hex_buffer_length, char_buffer, &char_buffer_length);
//         EXPECT_EQ(char_buffer_length, 0);
//     }
//     {
//         // Odd sized hex buffer
//         unsigned char hex_buffer[16] = {};
//         unsigned short hex_buffer_length = 15;
//         unsigned char char_buffer[8] = {};
//         unsigned short char_buffer_length = 8;
//         smu_hex_buffer_to_char_buffer(hex_buffer, hex_buffer_length, char_buffer, &char_buffer_length);
//         EXPECT_EQ(char_buffer_length, 0);
//     }
//     {
//         // Insuffisent length char buffer
//         unsigned char hex_buffer[16] = {};
//         unsigned short hex_buffer_length = 16;
//         unsigned char char_buffer[7] = {};
//         unsigned short char_buffer_length = 7;
//         smu_hex_buffer_to_char_buffer(hex_buffer, hex_buffer_length, char_buffer, &char_buffer_length);
//         EXPECT_EQ(char_buffer_length, 0);
//     }
// }
//
// TEST(utils, char_buffer_to_hex_buffer) {
//     {
//         // valid
//         unsigned char char_buffer[8] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef};
//         unsigned short char_buffer_length = 8;
//         unsigned char hex_buffer[20] = {};
//         unsigned short hex_buffer_length = 20;
//         smu_char_buffer_to_hex_buffer(char_buffer, char_buffer_length, hex_buffer, &hex_buffer_length);
//         EXPECT_EQ(hex_buffer_length, 16);
//         EXPECT_EQ(hex_buffer[0], '0');
//         EXPECT_EQ(hex_buffer[1], '1');
//         EXPECT_EQ(hex_buffer[2], '2');
//         EXPECT_EQ(hex_buffer[3], '3');
//         EXPECT_EQ(hex_buffer[4], '4');
//         EXPECT_EQ(hex_buffer[5], '5');
//         EXPECT_EQ(hex_buffer[6], '6');
//         EXPECT_EQ(hex_buffer[7], '7');
//         EXPECT_EQ(hex_buffer[8], '8');
//         EXPECT_EQ(hex_buffer[9], '9');
//         EXPECT_EQ(hex_buffer[10], 'A');
//         EXPECT_EQ(hex_buffer[11], 'B');
//         EXPECT_EQ(hex_buffer[12], 'C');
//         EXPECT_EQ(hex_buffer[13], 'D');
//         EXPECT_EQ(hex_buffer[14], 'E');
//         EXPECT_EQ(hex_buffer[15], 'F');
//     }
//
//     {
//         // Insufficient hex buffer size
//         unsigned char char_buffer[8] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef};
//         unsigned short char_buffer_length = 8;
//         unsigned char hex_buffer[14] = {};
//         unsigned short hex_buffer_length = 14;
//         smu_char_buffer_to_hex_buffer(char_buffer, char_buffer_length, hex_buffer, &hex_buffer_length);
//         EXPECT_EQ(hex_buffer_length, 0);
//     }
// }

TEST(utils, is_signaling_char) {
    for (int i = 0; i < 128; ++i) {
        if (1 <= i && i <= 4) {
            EXPECT_EQ(smu_is_signaling_char(i), 1);
        } else {
            EXPECT_EQ(smu_is_signaling_char(i), 0);
        }
    }
}

TEST(utils, is_hexadecimal_char) {
    for (int i = 0; i < 128; ++i) {
        if (0x30 <= i && i <= 0x39) {
            EXPECT_EQ(smu_is_hexadecimal_char(i), 1);
        } else if (0x41 <= i && i <= 0x46) {
            EXPECT_EQ(smu_is_hexadecimal_char(i), 1);
        } else {
            EXPECT_EQ(smu_is_hexadecimal_char(i), 0);
        }
    }
}

TEST(utils, is_acceptable_char) {
    for (int i = 0; i < 128; ++i) {
        if (1 <= i && i <= 4) {
            EXPECT_EQ(smu_is_acceptable_char(i), 1);
        } else if (0x30 <= i && i <= 0x39) {
            EXPECT_EQ(smu_is_acceptable_char(i), 1);
        } else if (0x41 <= i && i <= 0x46) {
            EXPECT_EQ(smu_is_acceptable_char(i), 1);
        } else {
            EXPECT_EQ(smu_is_acceptable_char(i), 0);
        }
    }
}

TEST(utils, is_header_valid) {
    {
        unsigned char buffer[] = {SOH, '0', '1', '2', '3', 'A', 'B', 'C', 'D', STX};
        serial_message_accumulator_t accumulator{
                .buffer = buffer,
                .length = PACKET_POS_STX,
        };
        EXPECT_EQ(smu_is_header_valid(&accumulator), 1);
    }
    {
        unsigned char buffer[] = {SOH, '\\', '0', '0', '0', '0', '0', '0', '0', STX};
        serial_message_accumulator_t accumulator{
                .buffer = buffer,
                .length = PACKET_POS_STX,
        };
        EXPECT_EQ(smu_is_header_valid(&accumulator), 0);
    }
    {
        unsigned char buffer[] = {SOH, '0', '0', '0', '0', '0', '0', '0'};
        serial_message_accumulator_t accumulator{
                .buffer = buffer,
                .length = 8,
        };
        EXPECT_EQ(smu_is_header_valid(&accumulator), 0);
    }
}

TEST(utils, is_payload_size_valid) {
    {
        unsigned char buffer[] = {SOH, '0', '0', '0', '0', '0', '0', '0', '0', STX};
        serial_message_accumulator_t accumulator{
                .buffer = buffer,
                .length = 10,
        };
        EXPECT_EQ(smu_is_payload_size_valid(&accumulator, 0), 1);
    }
    {
        unsigned char buffer[] = {SOH, '0', '0', '0', '0', '0', '2', '0', '0', STX, 'A', 'B'};
        serial_message_accumulator_t accumulator{
                .buffer = buffer,
                .length = 12,
        };
        EXPECT_EQ(smu_is_payload_size_valid(&accumulator, 0), 1);
    }
    {
        unsigned char buffer[] = {SOH, '0', '0', '0', '0', '0', '3', '0', '0', STX, 'A', 'B'};
        serial_message_accumulator_t accumulator{
                .buffer = buffer,
                .length = 12,
        };
        EXPECT_EQ(smu_is_payload_size_valid(&accumulator, 0), 0);
    }
    {
        unsigned char buffer[] = {SOH, '0', '0', '0', '0', '0', '1', '0', '0', STX, 'A', 'B'};
        serial_message_accumulator_t accumulator{
                .buffer = buffer,
                .length = 12,
        };
        EXPECT_EQ(smu_is_payload_size_valid(&accumulator, 0), 0);
    }
}

TEST(utils, is_in_crc_span) {
    unsigned char buffer[] = {SOH, '0', '0', '0', '0', '0', '0', '0', '0', STX,
                              ETX, '0', '0', '0', '0', EOT};
    serial_message_accumulator_t accumulator{.buffer = buffer};
    {
        // valid
        // 0
        accumulator.length = 10;
        EXPECT_EQ(smu_is_in_crc_span(&accumulator), 1);

        // 1
        accumulator.length = 11;
        EXPECT_EQ(smu_is_in_crc_span(&accumulator), 1);

        // 2
        accumulator.length = 12;
        EXPECT_EQ(smu_is_in_crc_span(&accumulator), 1);

        // 3
        accumulator.length = 13;
        EXPECT_EQ(smu_is_in_crc_span(&accumulator), 1);

        // 4
        accumulator.length = 14;
        EXPECT_EQ(smu_is_in_crc_span(&accumulator), 1);
    }

    {
        // too long
        accumulator.length = 15;
        EXPECT_EQ(smu_is_in_crc_span(&accumulator), 0);
    }

    {
        // too short
        accumulator.length = 9;
        EXPECT_EQ(smu_is_in_crc_span(&accumulator), 0);
    }
}
