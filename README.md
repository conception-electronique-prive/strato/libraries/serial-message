# Serial Message

C90 Utility library to send message over serial communication

It allows sending raw binary data over serial with error checking

C++23 bindings are also available (use `serial_message.hpp` instead of `serial_message.h`)

## Usage

### Using with cmake
Add the following to your CMakeList.txt:
```cmake
include(FetchContent)
FetchContent_Declare(
        serial_message
        GIT_REPOSITORY https://gitlab.com/conception-electronique-prive/strato/libraries/serial-message
)
FetchContent_MakeAvailable(serial_message)

# Your things...

target_link_libraries(${PROJECT_NAME} serial_message)
```

### Sender

* `serial_message_make_packet`: convert a byte array into a packet ready to be sent

### Receiver

* `serial_message_accumulate_byte`: accumulate and reconstruct a packet from a single byte
* `serial_message_accumulate_buffer`: accumulate and reconstruct a packet from a byte array
* `serial_message_decode_message`: decode message from a received packet

### Examples

an example is provided in the `examples` folder to showcase how to use the library

## Packet

A packet is composed by signaling bytes (orange), headers fields (blue), a payload (green) and a CRC (red)
![packet](doc/resource/packet.drawio.png)

signaling bytes are standard ASCII field for packet structure.

headers fields are message ID (unused) and payload size.

Payload size is used for checking packet integrity

Payload is the serialized message provided by user

CRC is a CRC16 for checking packet integrity.
