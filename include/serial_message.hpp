/**
 * @file    serial_message.hpp
 * @author  Samuel Martel
 * @date    2024-09-04
 * @brief
 *
 * @copyright
 * Copyright 2024 Paul Thomas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the “Software”), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#ifndef SERIAL_MESSAGE_HPP
#define SERIAL_MESSAGE_HPP
#include "serial_message.h"

#include <array>
#include <concepts>
#include <cstdint>
#include <expected>
#include <span>
#include <string_view>

namespace SerialMessage {
    constexpr std::string_view toString(serial_message_conversion_result_t r) {
        switch (r) {
            case SMCR_SUCCESS:
                return "Success";
            case SMCR_ERROR_INVALID_ACCUMULATOR:
                return "Invalid Accumulator";
            case SMCR_ERROR_BUFFER_TOO_SMALL:
                return "Buffer too small";
            default:
                return "Unknown";
        }
    }

    constexpr std::string_view toString(serial_message_accumulator_result_t r) {
        switch (r) {
            case SMAR_NO_MESSAGE:
                return "No message";
            case SMAR_HAS_MESSAGE:
                return "Has message";
            case SMAR_ERROR_ILL_FORMED_HEADER:
                return "Ill-formed header";
            case SMAR_ERROR_ILL_SIZED_HEADER:
                return "Ill-sized header";
            case SMAR_ERROR_ILL_FORMED_PAYLOAD:
                return "Ill-formed payload";
            case SMAR_ERROR_ILL_SIZED_PAYLOAD:
                return "Ill-sized payload";
            case SMAR_ERROR_ILL_FORMED_CRC:
                return "Ill-formed crc";
            case SMAR_ERROR_ILL_SIZED_CRC:
                return "Ill-sized crc";
            case SMAR_ERROR_INVALID_CRC:
                return "Invalid CRC";
            case SMAR_ERROR_BUFFER_FULL:
                return "Buffer full";
            default:
                return "Unknown";
        }
    }

    template<typename T>
    concept PhysicalInterface = requires(T t, int n) {
        // Interface must have a function that reads up to N bytes into a buffer, returning the number of bytes that
        // were actually received.
        { t.receive(static_cast<std::uint8_t*>(nullptr), n) } -> std::integral;

        // Interface must have a function that sends up to N bytes of data, returning the number of bytes that were
        // actually sent. If the value returned is less than N, this function will be called with the remaining data
        // until every byte have been sent.
        { t.send(static_cast<const std::uint8_t*>(nullptr), n) } -> std::integral;
    };

    /**
     * Straight up just a std::span with extra steps
     */
    class Packet {
    public:
        Packet(serial_message_accumulator_t* accumulator, std::span<std::uint8_t> data) :
            m_accumulator(accumulator), m_data(data) {}
        Packet(const Packet&) = delete;
        Packet(Packet&& o) noexcept : m_accumulator(o.m_accumulator), m_data(o.m_data) { o.m_accumulator = nullptr; }
        Packet& operator=(const Packet&) = delete;
        Packet& operator=(Packet&& o) noexcept {
            m_accumulator = o.m_accumulator;
            m_data = o.m_data;
            o.m_accumulator = nullptr;
            return *this;
        }
        ~Packet() {
            if (m_accumulator != nullptr) {
                serial_message_reset_accumulator(m_accumulator);
            }
        }

        auto begin() const noexcept -> decltype(auto) { return m_data.begin(); }
        auto cbegin() const noexcept -> decltype(auto) { return m_data.cbegin(); }
        auto end() const noexcept -> decltype(auto) { return m_data.end(); }
        auto cend() const noexcept -> decltype(auto) { return m_data.cend(); }
        auto rbegin() const noexcept -> decltype(auto) { return m_data.rbegin(); }
        auto crbegin() const noexcept -> decltype(auto) { return m_data.crbegin(); }
        auto rend() const noexcept -> decltype(auto) { return m_data.rend(); }
        auto crend() const noexcept -> decltype(auto) { return m_data.rend(); }

        auto front() const noexcept -> decltype(auto) { return m_data.front(); }
        auto back() const noexcept -> decltype(auto) { return m_data.back(); }
        auto operator[](std::size_t i) const noexcept -> decltype(auto) { return m_data[i]; }
        auto data() const noexcept -> decltype(auto) { return m_data.data(); }

        std::size_t size() const noexcept { return m_data.size(); }
        std::size_t size_bytes() const noexcept { return m_data.size_bytes(); }
        bool empty() const noexcept { return m_data.empty(); }

        template<std::size_t Count>
        std::span<std::uint8_t, Count> first() const {
            return m_data.first<Count>();
        }
        std::span<std::uint8_t> first(std::size_t count) const { return m_data.first(count); }
        template<std::size_t Count>
        std::span<std::uint8_t, Count> last() const {
            return m_data.last<Count>();
        }
        std::span<std::uint8_t> last(std::size_t count) const { return m_data.last(count); }

        template<std::size_t Offset, std::size_t Count = std::dynamic_extent>
        auto subspan() const -> decltype(auto) {
            return m_data.subspan<Offset, Count>();
        }
        std::span<std::uint8_t> subspan(std::size_t offset, std::size_t count = std::dynamic_extent) const {
            return m_data.subspan(offset, count);
        }

    private:
        serial_message_accumulator_t* m_accumulator = nullptr;
        std::span<std::uint8_t> m_data;
    };

    template<PhysicalInterface Inter, std::size_t BufferSize = 64>
    class Interface {
    public:
        template<typename... Args>
            requires std::constructible_from<Inter, Args...>
        explicit Interface(Args&&... args) noexcept(std::is_nothrow_constructible_v<Inter, Args...>) :
            m_interface(std::forward<Args>(args)...) {
            serial_message_init_accumulator(&m_accumulator, m_accumulatorBuffer.data(), m_accumulatorBuffer.size());
        }

        /**
         * Receives data from the physical interface, then checks if a packet is ready to be read.
         *
         * @note This currently assumes that only on packet can be received at a time on the physical interface.
         * @note This function must be called frequently.
         * @return SMAR_HAS_MESSAGE if a complete packet is ready to be read, an error code otherwise.
         */
        serial_message_accumulator_result_t process() noexcept(noexcept(m_interface.receive(nullptr, 0))) {
            std::uint16_t received = m_interface.receive(m_interfaceBuffer.data(), m_interfaceBuffer.size());
            if (received == 0) {
                return SMAR_NO_MESSAGE;
            }
            serial_message_accumulator_result_t result =
                    serial_message_accumulate_buffer(m_interfaceBuffer.data(), &received, &m_accumulator);
            return result;
        }

        /**
         * Processes and receives a complete packet from the physical interface.
         *
         * @note This function should only be called if `process` returns SMAR_HAS_MESSAGE.
         * @note Packets are not to be stored as-is. They are meant to be parsed as soon as returned from the function.
         * @return A Packet if one was received, an error code otherwise.
         */
        [[nodiscard]] std::expected<Packet, serial_message_conversion_result_t> receivePacket() noexcept {
            std::uint16_t size = m_interfaceBuffer.size();
            serial_message_conversion_result_t result =
                    serial_message_decode_message(&m_accumulator, m_interfaceBuffer.data(), &size);

            if (result != SMCR_SUCCESS) {
                return std::unexpected(result);
            }

            return Packet{&m_accumulator, std::span(m_interfaceBuffer.begin(), size)};
        }

        /**
         * Sends a packet on the interface.
         * @tparam T Any type that is Trivial (https://en.cppreference.com/w/cpp/named_req/TrivialType)
         * @tparam Count Size of the span, if the extent is statically known
         * @param data A span of data to be sent
         * @return SMCR_SUCCESS on success, error otherwise.
         */
        template<typename T, std::size_t Count = std::dynamic_extent>
        serial_message_conversion_result_t
        sendPacket(std::span<T, Count> data) noexcept(noexcept(m_interface.send(nullptr, 0))) {
            if (data.empty()) {
                return SMCR_SUCCESS;
            }
            std::uint16_t size = m_interfaceBuffer.size();
            auto res = serial_message_make_packet(reinterpret_cast<const std::uint8_t*>(data.data()), data.size_bytes(),
                                                  m_interfaceBuffer.data(), &size);
            if (res != SMCR_SUCCESS) {
                return res;
            }

            const std::uint8_t* ptr = m_interfaceBuffer.data();
            while (size > 0) {
                std::size_t written = m_interface.send(ptr, size);
                // Defend ourselves against a bad interface that doesn't return what we expect it to...
                if (written <= size) {
                    size -= written;
                    ptr += written;
                } else {
                    size = 0;
                }
            }

            return SMCR_SUCCESS;
        }

    private:
        Inter m_interface;
        serial_message_accumulator_t m_accumulator = {};
        std::array<std::uint8_t, BufferSize> m_accumulatorBuffer;
        std::array<std::uint8_t, BufferSize> m_interfaceBuffer;
    };
} // namespace SerialMessage
#endif // SERIAL_MESSAGE_HPP
