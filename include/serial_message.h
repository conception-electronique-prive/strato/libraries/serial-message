/**
 * @file    serial_message.h
 * @author  Paul Thomas
 * @date    5/24/2024
 * @brief
 *
 * @copyright
 * Copyright 2024 Paul Thomas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the “Software”), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef SERIAL_MESSAGE_H
#define SERIAL_MESSAGE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    SMCR_SUCCESS,
    SMCR_ERROR_INVALID_ACCUMULATOR,
    SMCR_ERROR_BUFFER_TOO_SMALL
} serial_message_conversion_result_t;

typedef enum {
    SMAR_NO_MESSAGE, /* No message detected during accumulation */
    SMAR_HAS_MESSAGE, /* Receive a valid message */
    SMAR_ERROR_ILL_FORMED_HEADER, /* Header has an invalid character */
    SMAR_ERROR_ILL_SIZED_HEADER, /* Message has an ill-sized header */
    SMAR_ERROR_ILL_FORMED_PAYLOAD, /* Payload has an invalid character */
    SMAR_ERROR_ILL_SIZED_PAYLOAD, /* Message has an ill-sized payload */
    SMAR_ERROR_ILL_FORMED_CRC,
    SMAR_ERROR_ILL_SIZED_CRC,
    SMAR_ERROR_INVALID_CRC, /* Received a message with an invalid CRC */
    SMAR_ERROR_BUFFER_FULL /* Reached end of accumulator buffer */
} serial_message_accumulator_result_t;

typedef struct {
    unsigned char *buffer;
    unsigned short max_length;
    unsigned short length;
    int (*_stage_handler)(unsigned char, void *);
} serial_message_accumulator_t;

/** Converts a message to a packet.
 *
 * packet_buffer_length allows to know whether the conversion went well or not.
 * Conversion fails if there isn't enough space to convert the message to a packet.
 *
 * You should provide 16 + N bytes for the conversion to succeed,
 * where N is the payload size and 16 is for 4 signaling bytes + 8 bytes of header + 4 bytes of CRC
 * @param message_buffer [IN] Buffer containing the message to convert
 * @param message_buffer_length [IN] Size of the buffer to convert
 * @param packet_buffer [OUT] Buffer to receive the packet from the message conversion
 * @param packet_buffer_length [IN-OUT] Size of the receiving buffer for the packet, return the final size of the
 * packet
 * @return conversion result
 */
serial_message_conversion_result_t serial_message_make_packet(const unsigned char *message_buffer,
                                                              unsigned short message_buffer_length,
                                                              unsigned char *packet_buffer,
                                                              unsigned short *packet_buffer_length);

/** Convert a packet to a message
 * @param accumulator [IN] the accumulator to be used to get the data
 * @param message_buffer [OUT] the buffer to receive the message
 * @param message_buffer_length [IN-OUT] tells the size of the buffer, then updated to the size of the message
 * @return conversion result
 */
serial_message_conversion_result_t serial_message_decode_message(const serial_message_accumulator_t *accumulator,
                                                                 unsigned char *message_buffer,
                                                                 unsigned short *message_buffer_length);

/** Properly populate an accumulator
 * @param accumulator [OUT] the accumulator to populate
 * @param buffer [IN] pointer to the buffer that will be borrowed by the accumulator
 * @param length [IN] the length of the buffer
 */
void serial_message_init_accumulator(serial_message_accumulator_t *accumulator, unsigned char *buffer,
                                     unsigned short length);

/** Reset an accumulator.
 *
 * To use after each success or failure from an accumulate function
 * @param accumulator [IN-OUT] the accumulator to reset
 */
void serial_message_reset_accumulator(serial_message_accumulator_t *accumulator);

/** Accumulate a byte and check if the accumulator contains a message
 * @param byte [IN] data received
 * @param accumulator [IN-OUT] the accumulator to use
 * @return tells if the accumulation went well or not
 */
serial_message_accumulator_result_t serial_message_accumulate_byte(unsigned char byte,
                                                                   serial_message_accumulator_t *accumulator);

/** Accumulate bytes and check if the accumulator contains a message.
 *
 * Does an early exit if a complete message (valid or not) was received before parsing all data.
 * buffer_length is updated to tell what was the last parsed index
 * @param buffer [IN] data received
 * @param buffer_length [IN-OUT] size of the received buffer,
 *                     updated to tell how many bytes were read
 *                     use it to tell if the full buffer was parsed or not
 * @param accumulator [IN-OUT] accumulator to use for the accumulation
 * @return tells if the accumulation went well or not
 */
serial_message_accumulator_result_t serial_message_accumulate_buffer(const unsigned char *buffer,
                                                                     unsigned short *buffer_length,
                                                                     serial_message_accumulator_t *accumulator);

#ifdef __cplusplus
}
#endif

#endif
