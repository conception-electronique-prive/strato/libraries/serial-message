/**
 * @file    fake_serial.c
 * @author  Paul Thomas
 * @date    5/26/2024
 * @brief
 *
 * @copyright
 * Copyright 2024 Paul Thomas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the “Software”), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "fake_serial.h"

static unsigned char serial_buffer[256];
static unsigned short serial_buffer_length = 0;

void user_serial_send(const unsigned char *buffer, unsigned short length) {
    int i = 0;
    for (; i < length; ++i) {
        serial_buffer[i] = buffer[i];
    }
    serial_buffer_length = length;
}

void user_serial_receive(unsigned char *buffer, unsigned short *length) {
    int i = 0;
    for (; i < serial_buffer_length; ++i) {
        buffer[i] = serial_buffer[i];
    }
    *length = serial_buffer_length;
    serial_buffer_length = 0;
}
