/**
 * @file    examples.c
 * @author  Paul Thomas
 * @date    5/26/2024
 * @brief
 *
 * @copyright
 * Copyright 2024 Paul Thomas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the “Software”), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "fake_serial.h"
#include "serial_message.h"


/* SENDING MACHINE */
static void sender() {
    /* The message we want to share
     * in this example, we provide a string, but it could be any serialized objet
     */
    const unsigned char* src_message = (const unsigned char*) "hello world";
    unsigned short src_message_length = 12;

    /* The receiving buffer required for storing the packet that will be sent over
     * buffer size must be at least: MESSAGE_SIZE * 2 + 16
     * In our examples, it then should be 12 * 2 + 16 = 40
     */
    unsigned char src_packet_buffer[256];
    unsigned short src_packet_buffer_length = 256;

    /* Generate the packet */
    serial_message_conversion_result_t make_result =
            serial_message_make_packet(src_message, src_message_length, src_packet_buffer, &src_packet_buffer_length);

    /* You must check whether the conversion went well or not
     * There can be two results
     * SMCR_SUCCESS -> Conversion went well
     * SMCR_ERROR_BUFFER_TOO_SMALL -> the provided buffer is not big enough for receiving the complete packet
     */
    if (make_result != SMCR_SUCCESS) {
        /* Conversion went wrong */
    }

    /* Send packet over serial interface */
    user_serial_send(src_packet_buffer, src_packet_buffer_length);
}

/* RECEIVING MACHINE */
static void receiver() {
    /* You must initialize one accumulator per serial interface
     * It is used to store received data and tell you once a full message has been received
     */
    unsigned char accumulator_buffer[256];
    serial_message_accumulator_t accumulator;

    serial_message_accumulator_result_t accumulator_result;
    unsigned char src_packet_buffer[256];
    unsigned short src_packet_buffer_length = 256;

    unsigned char dst_message_buffer[256];
    unsigned short dst_message_buffer_length = 256;
    serial_message_conversion_result_t decode_result;
    serial_message_init_accumulator(&accumulator, accumulator_buffer, 256);


    /* mock receiving data from sender  */

    user_serial_receive(src_packet_buffer, &src_packet_buffer_length);

    /* feed received data into accumulator
     * You can use serial_message_accumulate_buffer or serial_message_accumulate_byte
     */
    accumulator_result = serial_message_accumulate_buffer(src_packet_buffer, &src_packet_buffer_length, &accumulator);

    /* If there is not enough data to make a full packet, it will return SMAR_HAS_NO_MESSAGE
     * You then must continue receiving and feeding accumulator
     *
     * If there was enough data to make a full and valid packet, it will return SMAR_HAS_MESSAGE
     * Warning, if you were using accumulate_buffer, you must check src_packet_buffer_length as accumulate_buffer will
     * stop processing the input packet once it reaches either a valid packet or an error.
     *
     * If there was an error, it will return one of the many SMAR_ERROR enumeration values
     */

    if (accumulator_result != SMAR_HAS_MESSAGE) {
        /* in this example, we should have received a valid packet */
    }

    /* decode the received message */
    decode_result = serial_message_decode_message(&accumulator, dst_message_buffer, &dst_message_buffer_length);
    /* decode will check that the destination buffer has enough space to receive message and that the accumulator is in
     * a valid state */

    if (decode_result != SMCR_SUCCESS) {
        /* Conversion went wrong, must check why */
    }

    /* After receiving and parsing a packet, you must reset your accumulator */
    serial_message_reset_accumulator(&accumulator);
}


int main() {
    sender();
    receiver();

    return 0;
}
