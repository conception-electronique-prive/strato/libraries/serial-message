cmake_minimum_required(VERSION 3.11)
project(serial_message_example C)

add_executable(${PROJECT_NAME} example.c fake_serial.c)
target_link_libraries(${PROJECT_NAME} serial_message)

set(CMAKE_CXX_STANDARD 23)
add_executable(${PROJECT_NAME}-cxx cxx/example.cpp fake_serial.c)
target_link_libraries(${PROJECT_NAME}-cxx serial_message)
