/**
 * @file    example.cpp
 * @author  Samuel Martel
 * @date    2024-09-05
 * @brief
 *
 * @copyright
 * Copyright 2024 Paul Thomas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the “Software”), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <iostream>

#include <ostream>

#include "../fake_serial.h"
#include "serial_message.hpp"

#include <string_view>

struct Interface {
    size_t receive(uint8_t* data, size_t size) noexcept {
        unsigned short received = size;
        user_serial_receive(data, &received);
        return received;
    }

    size_t send(const uint8_t* data, size_t size) noexcept {
        user_serial_send(data, size);
        return size;
    }
};

namespace {
    // SENDING MACHINE
    void sender() {
        // Declare the interface we want to use.
        SerialMessage::Interface<Interface> interface;
        // The message we want to send:
        const char src[] = "Nya!~";

        // Send the message
        interface.sendPacket(std::span(src));
    }

    // RECEIVER MACHINE
    void receiver() {
        // Declare the interface we want to use.
        SerialMessage::Interface<Interface> interface;

        // process needs to be called periodically in order to receive data from the interface.
        // A packet must not be received until process returns SMAR_HAS_MESSAGE!
        if (auto res = interface.process(); res == SMAR_HAS_MESSAGE) {
            // The packet returned by receivePacket *must* be used before calling process or receivePacket again!
            auto pkt = interface.receivePacket();
            if (pkt.has_value()) {
                std::cout << "Received " << pkt.value().size() << " bytes:";
                for (auto&& c: pkt.value()) {
                    std::cout << c;
                }
                std::cout << std::endl;
            } else {
                std::cout << "Error when receiving packet: " << pkt.error() << std::endl;
            }
        } else if (res != SMAR_NO_MESSAGE) {
            // process will detect errors while receiving the data from the interface.
            std::cout << "Error when receiving packet: " << res << std::endl;
        }
    }
} // namespace

int main() {
    sender();
    receiver();
}
